﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class PlayerTurnManager : MonoBehaviour {
	public GameObject unityChan;
	public Canvas selectDirectionCanvas;
	public Canvas menuPanelCanvas;
	public Canvas displayMenuPanelCanvas;
	public Text progressText;
	public GameObject upButton;
	public GameObject rightButton;
	public GameObject downButton;
	public GameObject leftButton;

	// Use this for initialization
	void Start () {
		selectDirectionCanvas.enabled = false;
		menuPanelCanvas.enabled = false;
		displayMenuPanelCanvas.enabled = true;
		progressText.text = "";
		StartCoroutine ("GameStartProgress");
	}

	// シーン開始から方向ボタンが表示されるまでの流れをまとめたコルーチン
	private IEnumerator GameStartProgress() {
		unityChan.GetComponent<FaceAnim> ().ChangeFaceAnimation (0);
		progressText.text = "あっち…";
		yield return new WaitForSeconds (1.0f);
		unityChan.GetComponent<FaceAnim> ().ChangeFaceAnimation (1);
		progressText.text = "むいて…";
		yield return new WaitForSeconds (1.0f);
		unityChan.GetComponent<FaceAnim> ().ChangeFaceAnimation (2);
		// この時点でメニューパネルが表示されていたら方向ボタンを表示させない
		if (!menuPanelCanvas.enabled) {
			selectDirectionCanvas.enabled = true;
		}
	}
}