﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class VRModeButton : MonoBehaviour {
	private ButtonManager decisionGaze;
	public Cardboard VRMode;
	public Text modeChangingVRText;

	// Use this for initialization
	void Start () {
		VRModeKeeping ();
		decisionGaze = GetComponent<ButtonManager> ();
	}

	// Update is called once per frame
	void Update () {
		VRModeMenu (this.gameObject.name.Equals(decisionGaze.objectNameOfUseScript));
	}

	void VRModeMenu(bool decisionVRModeFlag) {
		if (decisionVRModeFlag) {
			decisionGaze.objectNameOfUseScript = null;
			if (VRSwitchKeepManager.VRModeSwitch) {
				// 2眼のときに単眼に切り替えるフラグを立てる
				VRSwitchKeepManager.VRModeSwitch = false;
			} else {
				// 単眼のときに2眼に切り替えるフラグを立てる
				VRSwitchKeepManager.VRModeSwitch = true;
			}
			SceneManager.LoadScene (SceneManager.GetActiveScene().buildIndex);	// シーンリセット
		}
	}

	void VRModeKeeping() {
		// VR視点切り替えボタンを押して
		// VRModeSwitch = true : 2眼
		// VRModeSwitch = false : 単眼
		if (VRSwitchKeepManager.VRModeSwitch) {
			VRMode.VRModeEnabled = true;
			modeChangingVRText.text = "VR:OFF";
		} else {
			VRMode.VRModeEnabled = false;
			modeChangingVRText.text = "VR:ON";
		}
	}
}
