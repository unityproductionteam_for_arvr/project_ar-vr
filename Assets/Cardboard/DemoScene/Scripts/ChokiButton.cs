﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class ChokiButton : MonoBehaviour {
	private ButtonManager decisionGaze;
	public JankenManager jankenManager;
	private bool decisionGoo = false;
	public GameObject unityChan;

	// Use this for initialization
	void Start () {
		decisionGaze = GetComponent<ButtonManager> ();
	}

	// Update is called once per frame
	void Update () {
		OnGooButton (this.gameObject.name.Equals(decisionGaze.objectNameOfUseScript), decisionGoo);
	}

	void OnGooButton(bool decisionGooFlag, bool goo) {
		if (decisionGooFlag && !goo) {
			jankenManager.progressText.text = "ポン!";
			jankenManager.gooButton.enabled = false;
			jankenManager.paaButton.enabled = false;
			int randomNum = (int)Random.Range (0f, 3f);
			decisionGoo = true;
			JudgeJanken (randomNum);
		}
	}

	void JudgeJanken(int jankenWay) {
		int sceneNum = 0;
		switch (jankenWay) {
		case 0:
			jankenManager.progressText.text = "グー!";
			unityChan.GetComponent<Animator> ().SetTrigger ("GooTrigger");
			sceneNum = 3;	// EnemyTurnに遷移
			break;
		case 1:
			jankenManager.progressText.text = "チョキ!";
			unityChan.GetComponent<Animator> ().SetTrigger ("ChokiTrigger");
			JankenManager.aikoFlag = true;
			sceneNum = SceneManager.GetActiveScene().buildIndex;	// ジャンケン画面に遷移
			break;
		case 2:
			jankenManager.progressText.text = "パー!";
			unityChan.GetComponent<Animator> ().SetTrigger ("PaaTrigger");
			sceneNum = 2;	// PlayerTurnに遷移
			break;
		default:
			jankenManager.progressText.text = "パー!";
			unityChan.GetComponent<Animator> ().SetTrigger ("PaaTrigger");
			sceneNum = 2;	// PlayerTurnに遷移
			break;
		}
		// ジャンケンの結果に応じたシーンに遷移するコルーチンを動かす
		StartCoroutine (JankenResult (sceneNum));
	}

	// ジャンケンの結果を受け取り、シーン遷移するコルーチン
	private IEnumerator JankenResult(int resultNum) {
		yield return new WaitForSeconds (2.0f);
		SceneManager.LoadScene (resultNum);	// シーンリセット
	}
}
