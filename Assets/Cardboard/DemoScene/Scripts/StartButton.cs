﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class StartButton : MonoBehaviour {
	private ButtonManager decisionGaze;

	// Use this for initialization
	void Start () {
		decisionGaze = GetComponent<ButtonManager> ();
	}

	// Update is called once per frame
	void Update () {
		OnToTitleButton (this.gameObject.name.Equals(decisionGaze.objectNameOfUseScript));
	}

	void OnToTitleButton(bool decisionStartFlag) {
		if (decisionStartFlag) {
			SceneManager.LoadScene (1);	// ジャンケン画面に遷移
		}
	}
}
