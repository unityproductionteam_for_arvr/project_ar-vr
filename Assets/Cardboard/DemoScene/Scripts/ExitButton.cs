﻿using UnityEngine;
using System.Collections;

public class ExitButton : MonoBehaviour {
	public bool exit;	// レティクルがStartボタンに触れたことを示すフラグ

	// Use this for initialization
	void Start () {
		ExitBool (false);	// レティクルがStartボタンに触れていない状態を保存する
	}

	// レティクルがExitボタンに触れているかどうかを設定する
	public void ExitBool(bool exitBool) {
		exit = exitBool;
	}

	// レティクルがExitボタンに触れているかどうかを返す
	public bool ExitAction() {
		return exit;
	}

	// レティクルがExitボタンに触れたことを示す
	public void OnGazeEnter() {
		ExitBool (true);
	}

	// レティクルがExitボタンに触れていない状態
	public void OnGazeExit() {
		ExitBool (false);
	}
}
