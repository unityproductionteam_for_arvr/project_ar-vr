﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using System.Collections;

public class ButtonManager : MonoBehaviour {
	// レティクルを各ボタンに当ててからカウントされる経過時間
	// ボタンからレティクルが離れるとカウントが0に戻る
	private float progressTime = 0f;
	public float progressLimitTime;		// Targettingが完了するまでの時間でインスペクター上で変更できる
	public Image progressCircleImage;
	// このスクリプトを組み込んでいるオブジェクト名ボタン個別の処理を呼び出すのに使う
	// 別スクリプトを挟んで使うがインスペクター上で変更できない
	[HideInInspector]
	public string objectNameOfUseScript;
	private Teleport gazeButton;

	// Use this for initialization
	void Start () {
		gazeButton = GetComponent<Teleport> ();	// レティクルの反応を操作するためにTeleportスクリプトコンポーネントを取得
		progressCircleImage.fillAmount = 0;		// 時間経過に伴い円形に表示する画像の表示割合
		progressCircleImage.enabled = false;	// circle画像を非表示
	}

	// Update is called once per frame
	void Update () {
		// ボタンにレティクルを当てるとボタンの処理をかけるまでの時間とcircle画像を表示
		Targetting (GazeThisButton());
	}

	// ボタンを選択しているかを判断するbool関数
	bool GazeThisButton() {
		if(gazeButton.gazedAtThisButton) {
			return true;
		}
		return false;
	}

	// ボタンにレティクルを当てることでTargettingの画像やテキストが表示される
	// レティクルを当て続けることでTargettingが進み
	// そのまま一定時間が経過するとみていたボタンに応じた処理がかかる
	void Targetting(bool isTargetting) {
		if (isTargetting) {
			// ボタンにレティクルを当て続けて一定時間が経過した
			if (progressTime >= progressLimitTime) {
//				progressCircleImage.enabled = false;
				// circle画像を完了する
				progressCircleImage.fillAmount = 1;
				// timeの値が増えすぎないようにするための予防
				progressTime = progressLimitTime;
				// ボタンを見続けて一定時間が経過すると
				// ボタンに応じた処理がかかる
				objectNameOfUseScript = GetObjectName(true);
			} else {
				// Targettingの画像やテキストが表示されて一定時間が経過するか
				// 途中でボタンからレティクルを離すまでTargetting画像の表示割合を増やす
				CircleAppear ();
			}
		} else {
			// ボタンからレティクルを離すとtargetting秒の経過時間が0となり
			// Targetting画像の表示割合もなくなって非表示になる
			ReticleTimeReset ();
		}
	}
		
	void CircleAppear() {
		// 残り時間を表す画像が非表示である場合、表示させる
		if (!progressCircleImage.enabled) {
			progressCircleImage.enabled = true;
		}
		// レティクルをボタンに当てている間の時間を経過させる
		progressTime += Time.deltaTime;
		// 時間経過に伴い円形に表示する画像の表示割合で最大値を1とする
		progressCircleImage.fillAmount = progressTime / progressLimitTime;
	}

	// レティクルをどのボタンにも当ててない状態にリセットする
	void ReticleTimeReset() {
		progressTime = 0f;	// ボタンをレティクルに当ててからの経過時間がリセットされる
		progressCircleImage.fillAmount = 0;	// Targetting画像の表示割合がリセットされる
		progressCircleImage.enabled = false;
		objectNameOfUseScript = GetObjectName (false);
	}

	public string GetObjectName(bool gazeButton) {
		if (gazeButton) {
			return this.name;
		}
		return null;
	}
}

