﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class ToTitleButton : MonoBehaviour {
	private ButtonManager decisionGaze;

	// Use this for initialization
	void Start () {
		decisionGaze = GetComponent<ButtonManager> ();
	}

	// Update is called once per frame
	void Update () {
		OnToTitleButton (this.gameObject.name.Equals(decisionGaze.objectNameOfUseScript));
	}

	void OnToTitleButton(bool decisionToTitleFlag) {
		if (decisionToTitleFlag) {
			SceneManager.LoadScene (0);	// タイトル画面に遷移
		}
	}
}
