﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class WayButton : MonoBehaviour {
	private ButtonManager decisionGaze;
	public JankenManager jankenTurnManager;
	private bool fixingWay = false;
	public GameObject unityChan;

	// Use this for initialization
	void Start () {
		decisionGaze = GetComponent<ButtonManager> ();
	}

	// Update is called once per frame
	void Update () {
		OnGooButton (this.gameObject.name.Equals(decisionGaze.objectNameOfUseScript),
						decisionGaze.objectNameOfUseScript, fixingWay);
	}

	void OnGooButton(bool decisionWayFlag, string PlayerWayType, bool fixingFlag) {
		if (decisionWayFlag && !fixingFlag) {
			jankenTurnManager.progressText.text = "ポン!";
			jankenTurnManager.gooButton.enabled = false;
			jankenTurnManager.chokiButton.enabled = false;
			jankenTurnManager.paaButton.enabled = false;
			string[] enemyWayType = { "GooButton", "ChokiButton", "PaaButton" };
			switch (PlayerWayType) {
			case "GooButton":
				Debug.Log ("Gooを選んだことになる");
				break;
			case "ChokiButton":
				break;
			case "PaaButton":
				break;
			default:
				break;
			}
			int randomNum = (int)Random.Range (0f, 3f);
			fixingWay = true;
			// ジャンケンの結果に応じたシーンに遷移するコルーチンを動かす
			StartCoroutine (JankenResult (JudgeJanken (enemyWayType[randomNum], PlayerWayType)));
		}
	}

	int JudgeJanken(string enemyWay, string playerWay) {
//		string[] jankenResult = { "あいこ", "勝ち", "負け" };
//		int sceneNum = 0;
		if (enemyWay == playerWay) {
			return 1;
		}
		return 2;
	}

	// ジャンケンの結果を受け取り、シーン遷移するコルーチン
	private IEnumerator JankenResult(int resultNum) {
		yield return new WaitForSeconds (2.0f);
		SceneManager.LoadScene (resultNum);	// シーンリセット
	}
}
