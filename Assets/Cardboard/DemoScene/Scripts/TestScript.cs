﻿using UnityEngine;
using System.Collections;

public class TestScript : MonoBehaviour {
	private Animator enemyAnim;

	// Use this for initialization
	void Start () {
		enemyAnim = GetComponent<Animator> ();
		ChangeMotion ();
	}

	public void ChangeMotion() {
		Debug.Log ("Goo");
		enemyAnim.SetBool ("Goo", true);
	}
}
