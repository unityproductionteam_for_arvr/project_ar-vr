﻿using UnityEngine;
using System.Collections;

public class MenuDisplayButton : MonoBehaviour {
	private ButtonManager decisionGaze;
	public JankenManager jankenManager;
	public PlayerTurnManager playerTurnManager;
	public EnemyTurnManager enemyTurnManager;

	// Use this for initialization
	void Start () {
		decisionGaze = GetComponent<ButtonManager> ();
	}

	// Update is called once per frame
	void Update () {
		OnMenuDisplayButton (this.gameObject.name.Equals(decisionGaze.objectNameOfUseScript));
	}

	void OnMenuDisplayButton(bool decisionMenuDisplayFlag) {
		if (decisionMenuDisplayFlag) {
			if (jankenManager != null) {
				jankenManager.menuPanelCanvas.enabled = true;
				jankenManager.progressText.enabled = false;
				jankenManager.selectWayCanvas.enabled = false;
			} else if (playerTurnManager != null) {
				playerTurnManager.menuPanelCanvas.enabled = true;
				playerTurnManager.progressText.enabled = false;
				playerTurnManager.selectDirectionCanvas.enabled = false;
			} else if (enemyTurnManager != null) {
				enemyTurnManager.menuPanelCanvas.enabled = true;
				enemyTurnManager.progressText.enabled = false;
				enemyTurnManager.selectDirectionCanvas.enabled = false;
			} else {
			}
			this.gameObject.SetActive(false);
		}
	}

	public void OnGazeClick() {
		Debug.Log("OnClickゲットだぜ!");
	}
}
