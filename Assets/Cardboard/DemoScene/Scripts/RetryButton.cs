﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class RetryButton : MonoBehaviour {
	private ButtonManager decisionGaze;

	// Use this for initialization
	void Start () {
		decisionGaze = GetComponent<ButtonManager> ();
	}

	// Update is called once per frame
	void Update () {
		OnToTitleButton (this.gameObject.name.Equals(decisionGaze.objectNameOfUseScript));
	}

	void OnToTitleButton(bool decisionToTitleFlag) {
		if (decisionToTitleFlag) {
			SceneManager.LoadScene (1);	// ジャンケン画面に遷移
		}
	}
}
