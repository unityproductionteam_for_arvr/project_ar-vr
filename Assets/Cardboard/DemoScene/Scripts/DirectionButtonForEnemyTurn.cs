﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class DirectionButtonForEnemyTurn : MonoBehaviour {
	private bool youLoseFlag;					// プレイヤーが負けたかどうかを判定するフラグ
	private ButtonManager decisionGaze;
	public EnemyTurnManager enemyTurnManager;
	private bool fixingDirection = false;

	// Use this for initialization
	void Start () {
		decisionGaze = GetComponent<ButtonManager> ();
	}

	// Update is called once per frame
	void Update () {
		OnGooButton (this.gameObject.name.Equals(decisionGaze.objectNameOfUseScript),
			decisionGaze.objectNameOfUseScript, fixingDirection);
	}

	void HideDirection() {
		enemyTurnManager.progressText.text = "ポン!";
		enemyTurnManager.upButton.SetActive(false);
		enemyTurnManager.rightButton.SetActive(false);
		enemyTurnManager.downButton.SetActive(false);
		enemyTurnManager.leftButton.SetActive(false);
	}

	void OnGooButton(bool decisionDirectionFlag, string PlayerDirectionType, bool fixingFlag) {
		if (decisionDirectionFlag && !fixingFlag) {
			HideDirection ();
			string[] enemyDirectionType = { "UpButton", "RightButton", "DownButton", "LeftButton" };
			string[] judge = { "勝ち", "負け" };
			switch (PlayerDirectionType) {
			case "UpButton":
				enemyTurnManager.upButton.SetActive(true);
				break;
			case "RightButton":
				enemyTurnManager.rightButton.SetActive(true);
				break;
			case "DownButton":
				enemyTurnManager.downButton.SetActive(true);
				break;
			case "LeftButton":
				enemyTurnManager.leftButton.SetActive(true);
				break;
			default:
				break;
			}
			int randomNum = (int)Random.Range (0f, 4f);
			fixingDirection = true;
			// ゲームの結果に応じてリザルト画面かジャンケン画面に遷移するコルーチンを動かす
			StartCoroutine (GameResult (JudgeGame (enemyDirectionType [randomNum], PlayerDirectionType)));
		}
	}

	int JudgeGame(string enemyDirection, string playerDirection) {
		switch (enemyDirection) {
		case "UpButton":
			enemyTurnManager.unityChan.GetComponent<Animator> ().SetBool ("UpArmTrigger", true);
			break;
		case "RightButton":
			enemyTurnManager.unityChan.GetComponent<Animator> ().SetBool ("LeftArmTrigger", true);
			break;
		case "DownButton":
			enemyTurnManager.unityChan.GetComponent<Animator> ().SetBool ("DownArmTrigger", true);
			break;
		case "LeftButton":
			enemyTurnManager.unityChan.GetComponent<Animator> ().SetBool ("RightArmTrigger", true);
			break;
		default:
			break;
		}
		// プレイヤーが向いた方向と相手が指した方向が一致したら
		// プレイヤーの負けという結果をもってリザルト画面に遷移する
		if (playerDirection == enemyDirection) {
			youLoseFlag = true;
			return 4;
		} else {
			youLoseFlag = false;
			return 1;
		}
	}

	// ゲームの結果を受け取り、シーン遷移するコルーチン
	private IEnumerator GameResult(int resultNum) {
		yield return new WaitForSeconds (2.0f);
		if (youLoseFlag) {
			VRSwitchKeepManager.gameEndFlag = false;
			enemyTurnManager.progressText.text = "あなたのまけ!";
		} else {
			enemyTurnManager.progressText.text = "もう1かい!";
		}
		yield return new WaitForSeconds (2.0f);
		SceneManager.LoadScene (resultNum);	// シーンリセット
	}
}
