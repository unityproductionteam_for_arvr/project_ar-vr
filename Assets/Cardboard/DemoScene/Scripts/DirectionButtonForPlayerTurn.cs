using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class DirectionButtonForPlayerTurn : MonoBehaviour {
	private bool youWinFlag;				// プレイヤーが勝ったかどうかを判定するフラグ
	private ButtonManager decisionGaze;
	public PlayerTurnManager playerTurnManager;
	private bool fixingDirection = false;

	// Use this for initialization
	void Start () {
		decisionGaze = GetComponent<ButtonManager> ();
	}

	// Update is called once per frame
	void Update () {
		OnGooButton (this.gameObject.name.Equals(decisionGaze.objectNameOfUseScript),
			decisionGaze.objectNameOfUseScript, fixingDirection);
	}

	void HideDirection() {
		playerTurnManager.progressText.text = "ポン!";
		playerTurnManager.upButton.SetActive(false);
		playerTurnManager.rightButton.SetActive(false);
		playerTurnManager.downButton.SetActive(false);
		playerTurnManager.leftButton.SetActive(false);
	}

	void OnGooButton(bool decisionDirectionFlag, string PlayerDirectionType, bool fixingFlag) {
		if (decisionDirectionFlag && !fixingFlag) {
			HideDirection ();
			string[] enemyDirectionType = { "UpButton", "RightButton", "DownButton", "LeftButton" };
			switch (PlayerDirectionType) {
			case "UpButton":
				playerTurnManager.upButton.SetActive(true);
				break;
			case "RightButton":
				playerTurnManager.rightButton.SetActive(true);
				break;
			case "DownButton":
				playerTurnManager.downButton.SetActive(true);
				break;
			case "LeftButton":
				playerTurnManager.leftButton.SetActive(true);
				break;
			default:
				break;
			}
			int randomNum = (int)Random.Range (0f, 4f);
			fixingDirection = true;
			// ジャンケンの結果に応じたシーンに遷移するコルーチンを動かす
			StartCoroutine (GameResult(JudgeGame (enemyDirectionType [randomNum], PlayerDirectionType)));
		}
	}

	int JudgeGame(string enemyDirection, string playerDirection) {
		switch (enemyDirection) {
		case "UpButton":
			playerTurnManager.unityChan.GetComponent<Animator> ().SetBool ("UpFaceTrigger", true);
			playerTurnManager.unityChan.GetComponent<Animator> ().SetBool ("UpFaceTrigger_Reverse", true);
			break;
		case "RightButton":
			playerTurnManager.unityChan.GetComponent<Animator> ().SetBool ("TurnLeftTrigger", true);
			playerTurnManager.unityChan.GetComponent<Animator> ().SetBool ("TurnLeftTrigger_Reverse", true);
			break;
		case "DownButton":
			playerTurnManager.unityChan.GetComponent<Animator> ().SetBool ("DownFaceTrigger", true);
			playerTurnManager.unityChan.GetComponent<Animator> ().SetBool ("DownFaceTrigger_Reverse", true);
			break;
		case "LeftButton":
			playerTurnManager.unityChan.GetComponent<Animator> ().SetBool ("TurnRightTrigger", true);
			playerTurnManager.unityChan.GetComponent<Animator> ().SetBool ("TurnRightTrigger_Reverse", true);
			break;
		default:
			break;
		}
		// プレイヤーが指した方向と相手の向いた方向が一致したら
		// プレイヤーの勝利という結果をもってリザルト画面に遷移する
		if (playerDirection == enemyDirection) {
			youWinFlag = true;
			return 4;
		} else {
			youWinFlag = false;
			return 1;
		}
	}

	// ゲームの結果を受け取り、シーン遷移するコルーチン
	private IEnumerator GameResult(int resultNum) {
		yield return new WaitForSeconds (1.0f);
		playerTurnManager.unityChan.GetComponent<Animator> ().SetBool ("IdleTrigger", true);
		yield return new WaitForSeconds (1.0f);
		if (youWinFlag) {
			playerTurnManager.unityChan.GetComponent<FaceAnim> ().ChangeFaceAnimation (3);
			VRSwitchKeepManager.gameEndFlag = true;
			playerTurnManager.progressText.text = "あなたのかち!";
		} else {
			playerTurnManager.unityChan.GetComponent<FaceAnim> ().ChangeFaceAnimation (4);
			playerTurnManager.progressText.text = "もう1かい!";
		}
		yield return new WaitForSeconds (2.0f);
		SceneManager.LoadScene (resultNum);	// シーンリセット
	}
}
