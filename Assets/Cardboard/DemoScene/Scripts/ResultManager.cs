﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class ResultManager : MonoBehaviour {
	public GameObject enemyET;
	public Canvas resultPanel;
	public Text progressText;

	// Use this for initialization
	void Start () {
		// ゲームの結果に合わせて対戦相手がアニメーションしたのち
		// メニューパネルを開く流れのコルーチン
		StartCoroutine ("ProgressResult");
	}

	// 勝利or敗北ポーズ
	void WinOrLose() {
		if (VRSwitchKeepManager.gameEndFlag) {	// 対戦相手が勝った
			progressText.text = "あなたのかち!";
			enemyET.GetComponent<Animator> ().SetBool ("LoseTrigger", true);
		} else {	// 対戦相手が負けた
			progressText.text = "あなたのまけ!";
			enemyET.GetComponent<Animator> ().SetBool ("WinTrigger", true);
		}
	}

	// ゲーム結果に対して対戦相手がリアクションしたのち
	// RETRY、TOTITLE、VR:ON/OFFのボタンが現れる
	private IEnumerator ProgressResult() {
		yield return new WaitForSeconds (0.5f);
		resultPanel.enabled = false;
		WinOrLose ();
		yield return new WaitForSeconds (2.0f);
		resultPanel.enabled = true;
	}
}
