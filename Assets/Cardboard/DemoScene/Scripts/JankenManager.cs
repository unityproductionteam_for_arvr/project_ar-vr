﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class JankenManager : MonoBehaviour {
	public static bool resultYouWinFlag;	// プレイヤーが勝ったかどうか判定するフラグ
	public static bool aikoFlag = false;	// ジャンケンの結果あいこになったかどうかを判定するフラグ
	public Canvas selectWayCanvas;
	public Canvas menuPanelCanvas;
	public Canvas displayMenuPanelCanvas;
	public Text progressText;
	public Button gooButton;
	public Button chokiButton;
	public Button paaButton;

	// Use this for initialization
	void Start () {
		// シーン開始時に
		// グー・チョキ・パーの画像とメニューパネルが非表示
		// メニューの出現ボタンは表示
		selectWayCanvas.enabled = false;
		menuPanelCanvas.enabled = false;
		displayMenuPanelCanvas.enabled = true;
		progressText.text = "";
		StartCoroutine(GameStartProgress(aikoFlag));
	}

	// シーン開始からグー・チョキ・パーボタンが表示されるまでの流れをまとめたコルーチン
	private IEnumerator GameStartProgress(bool aiko) {
		if (aiko) {
			progressText.text = "あい…";
			yield return new WaitForSeconds (1.0f);
			progressText.text = "こで…";
		} else {
			progressText.text = "ジャン…";
			yield return new WaitForSeconds (1.0f);
			progressText.text = "ケン…";
		}
		yield return new WaitForSeconds (2.0f);
		selectWayCanvas.enabled = true;
	}
}