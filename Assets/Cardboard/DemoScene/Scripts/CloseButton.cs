﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class CloseButton : MonoBehaviour {
	private ButtonManager decisionGaze;

	// Use this for initialization
	void Start () {
		decisionGaze = GetComponent<ButtonManager> ();
	}

	// Update is called once per frame
	void Update () {
		OnCloseButton (this.gameObject.name.Equals(decisionGaze.objectNameOfUseScript));
	}

	void OnCloseButton(bool decisionCloseFlag) {
		if (decisionCloseFlag) {
			SceneManager.LoadScene (SceneManager.GetActiveScene().buildIndex);	// シーンリセット
		}
	}
}
